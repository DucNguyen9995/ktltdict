#include "AppController.h"

#include <iostream>
#include <stdio.h>

using namespace std;

bool AppController::isRunning = true;
AppController * AppController::appController = nullptr;

AppController* AppController::sharedInstance() {
	if (appController == nullptr) {
		appController = new AppController();
	}
	return appController;
}

AppController::AppController() {
    cout << "=========******BKEncyclopedia******=========";
    theLoaiController = TheLoaiController::sharedInstance();
    wordController = WordController::sharedInstance();
}

AppController::~AppController() {

}

void AppController::run() {
    int choice;
    string temp;
    cout << "\nChon mot trong cac lua chon sau:" << endl;
    cout << "\t1. Tim kiem" << endl;
    cout << "\t2. Them tu" << endl;
    cout << "\t3. Them the loai" << endl;
    cout << "\t4. Thong ke tu theo the loai" << endl;
    cout << "\t5. Sua, xoa tu" << endl;
    cout << "\t6. Sua, xoa the loai (Warning: Tat ca cac tu cua the loai đo se bi xoa)" << endl;
    cout << "\t7. Thoat chuong trinh" << endl;
    
    cout << "Lua chon cua ban:\t";
    getline(cin, temp);
    choice = CommonUtils::getIntFromUser("Xin hay lua chon mot trong cac lua chon trong khoang 1 -> 7", temp);
    switch (choice) {
        case 1:
            // Search
            cout << "Ban muon tim kiem gi?" << endl;
            cout << "\t1. Tim kiem tu" << endl;
            cout << "\t2. Tim kiem the loai" << endl;
            cout << "Lua chon cua ban:\t";
            getline(cin, temp);
            choice = CommonUtils::getIntFromUser("Xin hay lua chon mot trong cac lua chon 1 hoac 2: ", temp);
            {
                clock_t clock = CommonUtils::start();
                switch (choice) {
                    case 1:
                        // Tu
                        cout << "Nhap Tu:\t";
                        getline(cin, temp);
                        WordController::sharedInstance()->searchWord(temp);
                        break;
                    case 2:
                        // The Loai
                        TheLoaiController::sharedInstance()->searchTheLoai();
                        break;
                        
                    default:
                        cout << "Ke me ban. ban khon nan vl";
                        break;
                }
                CommonUtils::stopTimer(clock);
            }
            break;
        case 2:
            // Add Word
            {
                clock_t clock = CommonUtils::start();
                wordController->newWord();
                CommonUtils::stopTimer(clock);
            }
            break;
        case 3:
            // Add Type
            {
                clock_t clock = CommonUtils::start();
                theLoaiController->newTheLoai();
                CommonUtils::stopTimer(clock);
            }
            break;
        case 4:
            // statistic of Word in a type
            cout << "Nhap vao ma the loai hoac the loai:\t";
            getline(cin, temp);
            {
                clock_t clock = CommonUtils::start();
                wordController->wordStatistic(temp);
                CommonUtils::stopTimer(clock);
            }
            break;
        case 5:
            // Update Word
            cout << "Ban Muon xoa hay sua tu?" << endl;
            cout << "\t1. Sua" << endl;
            cout << "\t2. Xoa" << endl;
            cout << "Lua chon cua ban:\t";
            getline(cin, temp);
            choice = CommonUtils::getIntFromUser("Xin hay lua chon so.", temp);
            cout << "Hay nhap tu:\t";
            getline(cin, temp);
            {
                clock_t clock = CommonUtils::start();
                switch (choice) {
                    case 1:
                        if (!wordController->modifyWord(temp)) {
                            cout << "Qua trinh sua bi loi hoac tu nay khong ton tai" << endl;
                        }
                        break;
                    case 2:
                        if (!wordController->modifyWord(temp, kWordModifyKeyDelete)) {
                            cout << "Qua trinh xoa bi loi hoac tu nay khong ton tai" << endl;
                        }
                        break;
                        
                    default:
                        break;
                }
                CommonUtils::stopTimer(clock);
            }
            break;
        case 6:
            // Update Type
            cout << "Ban Muon xoa hay sua the loai?" << endl;
            cout << "\t1. Sua" << endl;
            cout << "\t2. Xoa" << endl;
            cout << "Lua chon cua ban:\t";
            getline(cin, temp);
            choice = CommonUtils::getIntFromUser("Xin hay lua chon so", temp);
            cout << "Hay nhap ten the loai hoac ma the loai:\t";
            getline(cin, temp);
            {
                clock_t clock = CommonUtils::start();
                switch (choice) {
                    case 1:
                        if (!theLoaiController->modifyTheLoai(temp)) {
                            cout << "Qua trinh sua bi loi hoac tu nay khong co trong tu dien" << endl;
                        }
                        break;
                    case 2:
                        if (!theLoaiController->modifyTheLoai(temp, kTheLoaiModifyKeyDelete)) {
                            cout << "Qua trinh xoa bi loi hoac tu nay khong co trong tu dien" << endl;
                        }
                        break;
                        
                    default:
                        break;
                }
                CommonUtils::stopTimer(clock);
            }
            break;
        case 7:
            isRunning = false;
            {
                clock_t clock = CommonUtils::start();
                theLoaiController->writeToFile();
                wordController->writeToFile();
                CommonUtils::stopTimer(clock);
            }
            break;
            
        default:
            break;
    }
}