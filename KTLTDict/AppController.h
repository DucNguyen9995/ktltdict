#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include "TheLoaiController.h"

class AppController {
public:
    static AppController* sharedInstance();
	~AppController();
    TheLoaiController* theLoaiController;
    WordController* wordController;
    void run();
    static bool isRunning;
private:
	static AppController *appController;
	AppController();
};

#endif