#include "CommonUtils.h"
#include <iostream>
#include <iterator>
#include <cstdlib>
#include <cstdio>
#include <iomanip>

using namespace std;

vector<string> &CommonUtils::split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        if (item == "") {
            continue;
        }
        elems.push_back(item);
    }
    return elems;
}

vector<string> CommonUtils::split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

void CommonUtils::testCoredump() {
	cout << "ok input test";
	string test;
    getline(cin, test);
}

string CommonUtils::join(vector<string> elems, char delim) {
	string str = "";
    for (vector<string>::const_iterator iterator = elems.begin(); iterator!= elems.end(); iterator++) {
    	str += *iterator;
		if (iterator == elems.end()) {
			str += "";
		} else {
			str += delim;
		}
	}
	return str;
}

clock_t CommonUtils::start() {
    clock_t timer;
    timer = clock();
    return timer;
}

void CommonUtils::stopTimer(time_t start) {
    cout << "Thoi gian thuc hien mat:" << setprecision(3) << fixed << (clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << "ms" << endl;
}

int CommonUtils::getIntFromUser(string message, string &temp) {
    while (atoi(temp.c_str()) == 0 && temp != "0") {
        cout << message << ":\t";
        getline(cin, temp);
        cin.clear();
    }
    return atoi(temp.c_str());
}

bool CommonUtils::replace(string& str, const string& from, const string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}