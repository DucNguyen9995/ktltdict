#ifndef COMMONUTILS_H
#define COMMONUTILS_H

#include <string>
#include <sstream>
#include <time.h>
#include <functional>
#include "hashtable.cpp"


using namespace std;

class CommonUtils {
public:
	static vector<string> split(const string &s, char delim);
	static void testCoredump();
	static string join(vector<string> elems, char delim);
    static clock_t start();
    static void stopTimer(time_t start);
    static int getIntFromUser(string message, string &temp);
    static bool replace(string& str, const string& from, const string& to);
private:
	static vector<string> &split(const string &s, char delim, vector<string> &elems);
};
#endif