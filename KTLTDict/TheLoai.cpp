#include "TheLoai.h"
#include <iostream>
#include <climits>

using namespace std;

TheLoai::TheLoai() {
	maTheLoai = 0;
	tenTheLoai = "";
    isModified = true;
}

TheLoai::TheLoai(int ma, string ten, string cacTruong) {
	maTheLoai = ma;
	tenTheLoai = ten;
	cacTruongCuaTheLoai = CommonUtils::split(cacTruong, ';');
}

void TheLoai::nhapTheLoai(int maTheLoai) {
    string temp;
    if (!maTheLoai) {
        cout << "Nhap Ma The Loai:\t";
        getline(cin, temp);
        cin.clear();
        while (atoi(temp.c_str()) == 0 && temp != "0") {
            cout << "Ma the loai phai la so. Nhap lai:\t";
            getline(cin, temp);
            cin.clear();
        }
        this->maTheLoai = atoi(temp.c_str());
    } else {
        this->maTheLoai = maTheLoai;
    }
    cin.clear();
    cout << "Nhap Ten The Loai:\t";
    getline(cin, temp);
    tenTheLoai = temp;
    cin.clear();
	cout << "Nhap Cac Truong Cua The Loai " << tenTheLoai << "(Ten cac truong phan cach nhau boi dau ;):\n";
    getline(cin, temp);
    cin.clear();
    cacTruongCuaTheLoai = CommonUtils::split(temp, ';');
}