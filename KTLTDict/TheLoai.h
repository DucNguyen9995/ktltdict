#ifndef THELOAI_H
#define THELOAI_H

#include <cstdlib>
#include <string>
#include "CommonUtils.h"

using namespace std;

class TheLoai {
public:
	TheLoai();
	TheLoai(int ma, string ten, string cacTruong);
	int maTheLoai;
    long line;
	string tenTheLoai;
	vector<string> cacTruongCuaTheLoai;
    bool isModified;
	void nhapTheLoai(int maTheLoai = 0);
//	bool &operator==;
};
#endif