#include "TheLoaiController.h"

#include <fstream>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include "CommonUtils.h"
#include <algorithm>

using namespace std;

TheLoaiController *TheLoaiController::theLoaiController = NULL;

TheLoaiController* TheLoaiController::sharedInstance() {
	if (theLoaiController == nullptr) {
		theLoaiController = new TheLoaiController();
	}
	return theLoaiController;
}

TheLoaiController::TheLoaiController() {
    databaseModified = false;
    isFirstTimeRun = true;
	struct stat buffer;
	ifstream input;
	if (stat (kTheLoaiPositionFile, &buffer) == 0) {
        input.open(kTheLoaiPositionFile);
        if (input.is_open()) {
            isFirstTimeRun = false;
        }
    }
    
    if (isFirstTimeRun) {
        readAllType(input);
    } else {
        if (input.is_open()) {
            while (!input.eof()) {
                string str;
                getline(input, str);
                if (str == "") continue;
                TheLoai *theLoai = new TheLoai();
                theLoai->tenTheLoai = str;
                getline(input, str);
                theLoai->maTheLoai = atoi(str.c_str());
                getline(input, str);
                theLoai->line = atoi(str.c_str());
                theLoaiList.push_back(theLoai);
                ostringstream oStrStream;
                oStrStream << theLoai->maTheLoai;
                tblMaTheLoai[oStrStream.str().c_str()] = theLoai;
                tblTheLoai[theLoai->tenTheLoai.c_str()] = theLoai;
            }
        }
    }
}

void TheLoaiController::readAllType(ifstream &input) {
    struct stat buffer;
    if (stat (kTheLoaiDatabase, &buffer) == 0) {
        input.open(kTheLoaiDatabase);
    }
    
    if (input.is_open()) {
        while (!input.eof()) {
            string str;
            getline(input, str);
            if (str.empty() || str.c_str()[0] <= 32) {
                break;
            }
            TheLoai *theLoai = new TheLoai();
            theLoai->maTheLoai = atoi(str.c_str());
            if (!theLoai->maTheLoai)
                break;
            getline(input, str);
            theLoai->tenTheLoai = str;
            getline(input, str);
            theLoai->cacTruongCuaTheLoai = CommonUtils::split(str,';');
            theLoaiList.push_back(theLoai);
            ostringstream oStrStream;
            oStrStream << theLoai->maTheLoai;
            tblMaTheLoai[oStrStream.str().c_str()] = theLoai;
            tblTheLoai[theLoai->tenTheLoai.c_str()] = theLoai;
        }
        input.close();
    }
}

bool TheLoaiController::readATypeFromFile(TheLoai &theLoai) {
    if (theLoai.cacTruongCuaTheLoai.size())
        return true;
    ifstream input;
    input.open(kTheLoaiDatabase);
    if (!input) {
        return false;
    }
    input.seekg(theLoai.line);
    string temp;
    getline(input, temp);
    theLoai.maTheLoai = atoi(temp.c_str());
    getline(input, temp);
    theLoai.tenTheLoai = temp;
    getline(input, temp);
    theLoai.cacTruongCuaTheLoai = CommonUtils::split(temp, ';');
    return true;
}

bool TheLoaiController::writeToFile() {
    if (databaseModified || isFirstTimeRun) {
        if (databaseModified) {
            for (vector<TheLoai*>::const_iterator iterator = theLoaiList.begin(); iterator != theLoaiList.end(); iterator++) {
                readATypeFromFile(*(*iterator));
            }
        }
        ofstream outStream(kTheLoaiDatabase, ios_base::out);
        ofstream outPosition(kTheLoaiPositionFile, ios_base::out);
        if (!outStream || !outPosition) {
            outStream.close();
            outPosition.close();
            return false;
        }
        for (vector<TheLoai*>::const_iterator iterator = theLoaiList.begin(); iterator != theLoaiList.end(); iterator++) {
            outPosition << (*iterator)->tenTheLoai << endl << (*iterator)->maTheLoai << endl;
            outPosition << outStream.tellp() << endl;
            outStream << (*iterator)->maTheLoai << endl;
            outStream << (*iterator)->tenTheLoai << endl;
            outStream << CommonUtils::join(((*iterator)->cacTruongCuaTheLoai),';') << endl;
        }
        outStream.close();
        outPosition.close();
    }
    return true;
}

bool TheLoaiController::newTheLoai() {
    TheLoai *theLoai = new TheLoai();
    theLoai->nhapTheLoai();
    ostringstream oStrStream;
    oStrStream << theLoai->maTheLoai;
    while (tblTheLoai[theLoai->tenTheLoai.c_str()]) {
        cout << "The loai nay da ton tai.\nXin hay nhap lai ten the loai: ";
        string temp;
        getline(cin, temp);
        cin.clear();
        theLoai->tenTheLoai = temp;
    }
    while (tblMaTheLoai[oStrStream.str().c_str()]) {
        cout << "Ma the loai nay da ton tai.\nXin hay nhap lai ma the loai: ";
        string temp;
        getline(cin, temp);
        cin.clear();
        if (!(atoi(temp.c_str()) == 0) && temp != "0")
            theLoai->maTheLoai = atoi(temp.c_str());
        oStrStream.str("");
        oStrStream << theLoai->maTheLoai;
    }
    oStrStream.str("");
    oStrStream << theLoai->maTheLoai;
    string str = oStrStream.str();
    tblMaTheLoai[oStrStream.str().c_str()] = theLoai;
    tblTheLoai[theLoai->tenTheLoai.c_str()] = theLoai;
    theLoaiList.push_back(theLoai);
    databaseModified = true;
    return databaseModified;
}

void TheLoaiController::searchTheLoai() {
    cout << "Nhap ten the loai hoac ma the loai: ";
    string temp;
    getline(cin, temp);
    TheLoai *theLoai = nullptr;
    if (atoi(temp.c_str()) == 0 && temp != "0") {
        theLoai = tblTheLoai[temp.c_str()];
    } else {
        theLoai = tblMaTheLoai[temp.c_str()];
    }
    if (!theLoai) {
        cout << "Khong co the loai nay trong tu dien." << endl;
        cout << "Bam Enter/Return de tiep tuc." << endl;
        getline(cin, temp);
        cin.clear();
    } else {
        if (!isFirstTimeRun) {
            if (!theLoai->cacTruongCuaTheLoai.size()) {
                ifstream input;
                input.open(kTheLoaiDatabase);
                if (!input) {
                    cout << "File Missing";
                    return;
                } else {
                    input.seekg(theLoai->line);
                    getline(input, temp);
                    getline(input, temp);
                    getline(input, temp);
                    theLoai->cacTruongCuaTheLoai = CommonUtils::split(temp, ';');
                }
            }
        }
        cout << "Ma The Loai:\t" << theLoai->maTheLoai << endl;
        cout << "Ten The Loai:\t" << theLoai->tenTheLoai << endl;
        cout << "Cac truong cua the loai:" << endl;
        for (vector<string>::const_iterator iterator = theLoai->cacTruongCuaTheLoai.begin(); iterator != theLoai->cacTruongCuaTheLoai.end(); iterator++) {
            cout << *iterator << endl;
        }
    }
}

bool TheLoaiController::modifyTheLoai(string key, string mode) {
    if (!tblTheLoai[key.c_str()] && !tblMaTheLoai[key.c_str()]) {
        return false;
    }
    cout << "Are you sure(y/n)?\t";
    string temp;
    getline(cin, temp);
    if (temp == "y" || temp == "Y") {
        TheLoai *tempTheLoai = nullptr;
        if (tblTheLoai[key.c_str()]) {
            tempTheLoai = tblTheLoai[key.c_str()];
        }
        if (tblMaTheLoai[key.c_str()]) {
            tempTheLoai = tblMaTheLoai[key.c_str()];
        }
        if (mode == kTheLoaiModifyKeyChange) {
            int tempMaTheLoai = tempTheLoai->maTheLoai;
            WordController::sharedInstance()->removeWordWithMaTheLoai(tempMaTheLoai);
            TheLoai *theLoai = new TheLoai();
            theLoai->nhapTheLoai(tempMaTheLoai);
            ostringstream oss;
            oss << tempMaTheLoai;
            temp = tblMaTheLoai[oss.str().c_str()]->tenTheLoai;
            theLoaiList.erase(remove(theLoaiList.begin(), theLoaiList.end(), tblMaTheLoai[oss.str().c_str()]));
            tblTheLoai[tblMaTheLoai[oss.str().c_str()]->tenTheLoai.c_str()] = nullptr;
            tblMaTheLoai[oss.str().c_str()] = nullptr;
            tblMaTheLoai[oss.str().c_str()] = theLoai;
            tblTheLoai[theLoai->tenTheLoai.c_str()] = theLoai;
            theLoaiList.push_back(theLoai);
        } else {
            int tempMaTheLoai = tempTheLoai->maTheLoai;
            WordController::sharedInstance()->removeWordWithMaTheLoai(tempMaTheLoai);
            ostringstream oss;
            oss << tempMaTheLoai;
            temp = tblMaTheLoai[oss.str().c_str()]->tenTheLoai;
            theLoaiList.erase(remove(theLoaiList.begin(), theLoaiList.end(), tblMaTheLoai[oss.str().c_str()]));
            tblTheLoai[tblMaTheLoai[oss.str().c_str()]->tenTheLoai.c_str()] = nullptr;
            tblMaTheLoai[oss.str().c_str()] = nullptr;
        }
    }
    databaseModified = true;
    return true;
}