#ifndef THELOAICONTROLLER_H
#define THELOAICONTROLLER_H


#include "TheLoai.h"
#include "WordController.h"

#define kTheLoaiValueFilename 			"InputValue.txt"
#define kTheLoaiDatabase				"TheLoaiDatabase.txt"
#define kTheLoaiPositionFile            "TheLoaiPositionFile.yolo"

#define kTheLoaiModifyKeyChange         "Change"
#define kTheLoaiModifyKeyDelete         "Delete"

using namespace std;

class TheLoaiController {
public:
    bool databaseModified;
    bool isFirstTimeRun;
	static TheLoaiController* sharedInstance();
	vector<TheLoai*> theLoaiList;
    hashtable<TheLoai *> tblTheLoai;
    hashtable<TheLoai *> tblMaTheLoai;
    bool newTheLoai();
    bool writeToFile();
    void searchTheLoai();
    bool modifyTheLoai(string tenTheLoai, string mode = kTheLoaiModifyKeyChange);
    static bool readATypeFromFile(TheLoai &theLoai);
private:
    void readAllType(ifstream &input);
	static TheLoaiController *theLoaiController;
	TheLoaiController();
};

#endif