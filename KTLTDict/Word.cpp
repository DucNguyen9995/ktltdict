//
//  Word.cpp
//  KTLT
//
//  Created by Duc Nguyen on 4/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#include <iostream>
#include "Word.h"
#include "TheLoaiController.h"
#include "WordController.h"

using namespace std;

Word::Word() {
    tu = "";
    maTheLoai = -1;
    tblFieldDetail = new hashtable<string>(16);
    isModified = false;
}

bool Word::nhapWord(int maTheLoai) {
    cout << "Nhap Tu:\t";
    getline(cin, tu);
    while (WordController::sharedInstance()->tblWord[tu.c_str()]) {
        cout << "Tu nay da ton tai xin hay nhap lai tu:\t";
        getline(cin, tu);
    }
    if (!maTheLoai) {
        cout << "Nhap Ma The Loai cua Tu " << tu << ":\t";
        string temp;
        getline(cin, temp);
        cin.clear();
        while (atoi(temp.c_str()) == 0 && temp != "0") {
            cout << "Ma the loai phai la so. Nhap lai:\t";
            getline(cin, temp);
            cin.clear();
        }
        this->maTheLoai = atoi(temp.c_str());
    } else {
        this->maTheLoai = maTheLoai;
    }
    ostringstream oStrStream;
    oStrStream << this->maTheLoai;
    TheLoai* theLoai = TheLoaiController::sharedInstance()->tblMaTheLoai[oStrStream.str().c_str()];
    if (!theLoai) {
        cout << "khong ton tai the loai nay!";
        return false;
    }
    TheLoaiController::readATypeFromFile(*theLoai);
    vector<string> vectorTruong = theLoai->cacTruongCuaTheLoai;
    for (vector<string>::const_iterator iterator = vectorTruong.begin(); iterator != vectorTruong.end(); iterator++) {
        cout << "Nhap du lieu cho truong " << *iterator << ":\t";
        getline(cin, (*tblFieldDetail)[(*iterator).c_str()]);
        cin.clear();
    }
    isModified = true;
    return true;
}