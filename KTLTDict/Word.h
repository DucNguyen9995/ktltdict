//
//  Word.h
//  KTLT
//
//  Created by Duc Nguyen on 4/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#ifndef Word_h
#define Word_h

#include <string>
#include "CommonUtils.h"

class Word {
public:
    Word();
    string tu;
    long line;
    int maTheLoai;
    hashtable<string> *tblFieldDetail;
    bool isModified;
    bool nhapWord(int maTheLoai = 0);
private:
    
};

#endif /* Word_hpp */
