//
//  WordController.cpp
//  KTLT
//
//  Created by Duc Nguyen on 4/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#include "WordController.h"

#include <fstream>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <vector>
#include "TheLoaiController.h"

using namespace std;

WordController *WordController::wordController = nullptr;

WordController *WordController::sharedInstance() {
    if (!wordController) {
        wordController = new WordController();
    }
    return wordController;
}

WordController::WordController() {
    isFirstTimeRun = true;
    databaseModified = false;
    struct stat buffer;
    ifstream input;
    if (stat (kWordPositionFile, &buffer) == 0) {
        input.open(kWordPositionFile);
        if (input.is_open()) {
            isFirstTimeRun = false;
        }
    }
    
    if (isFirstTimeRun) {
        readAllWord(input);
    } else {
        if (input.is_open()) {
            while (!input.eof()) {
                string str;
                getline(input, str);
                if (str == "") break;
                Word *word = new Word();
                word->tu = str;
                getline(input, str);
                word->maTheLoai = atoi(str.c_str());
                getline(input, str);
                word->line = atoi(str.c_str());
                wordList.push_back(word);
                tblWord[word->tu.c_str()] = word;
            }
        }
        input.close();
    }
}

void WordController::readAllWord(ifstream &input) {
    struct stat buffer;
    if (stat (kWordDatabase, &buffer) == 0) {
        input.open(kWordDatabase);
    }
    
    
    if (input.is_open()) {
        while (!input.eof()) {
            string str;
            getline(input, str);
            if (str.empty() || str.c_str()[0] <= 32) {
                break;
            }
            Word *word = new Word();
            word->tu = str;
            getline(input, str);
            word->maTheLoai = atoi(str.c_str());
            hashtable<TheLoai *> tblMaTheLoai = TheLoaiController::sharedInstance()->tblMaTheLoai;
            ostringstream oStrStream;
            oStrStream << word->maTheLoai;
            TheLoai* theLoai = tblMaTheLoai[oStrStream.str().c_str()];
            if (!theLoai)
                break;
            TheLoaiController::sharedInstance()->readATypeFromFile(*theLoai);
            vector<string> cacTruong = theLoai->cacTruongCuaTheLoai;
            for (vector<string>::const_iterator iterator = cacTruong.begin(); iterator != cacTruong.end(); iterator++) {
                getline(input, str);
                (*(word->tblFieldDetail))[(*iterator).c_str()] = str;
            }
            wordList.push_back(word);
            tblWord[word->tu.c_str()] = word;
        }
        input.close();
    }
}

bool WordController::readAWordFromFile(Word &word) {
    ifstream input;
    input.open(kWordDatabase);
    if (!input) {
        return false;
    }
    input.seekg(word.line);
    string temp;
    getline(input, temp);
    word.tu = temp;
    getline(input, temp);
    word.maTheLoai = atoi(temp.c_str());
    ostringstream oStrStream;
    oStrStream << word.maTheLoai;
    string str = oStrStream.str();
    TheLoai* theLoai = TheLoaiController::sharedInstance()->tblMaTheLoai[oStrStream.str().c_str()];
    TheLoaiController::sharedInstance()->readATypeFromFile(*theLoai);
    vector<string> cacTruong = theLoai->cacTruongCuaTheLoai;
    for (vector<string>::const_iterator iterator = cacTruong.begin(); iterator != cacTruong.end(); iterator++) {
        getline(input, temp);
        (*(word.tblFieldDetail))[(*iterator).c_str()] = temp;
    }
    return true;
}

bool WordController::writeToFile() {
    if (databaseModified || isFirstTimeRun) {
        if (databaseModified) {
            for (vector<Word*>::const_iterator iterator = wordList.begin(); iterator != wordList.end(); iterator++) {
                if ((*iterator)->isModified) {
                    continue;
                }
                if ((*iterator)->maTheLoai == 0) {
                    continue;
                }
                readAWordFromFile(*(*iterator));
            }
        }
        ofstream outStream(kWordDatabase);
        ofstream outPos(kWordPositionFile);
        if (!outStream || !outPos) {
            outStream.close();
            outPos.close();
            return false;
        }
        for (vector<Word*>::const_iterator iterator = wordList.begin(); iterator != wordList.end(); iterator++) {
            outPos << (*iterator)->tu << endl << (*iterator)->maTheLoai << endl;
            outPos << outStream.tellp() << endl;
            outStream << (*iterator)->tu << endl;
            outStream << (*iterator)->maTheLoai << endl;
            hashtable<TheLoai *> tblMaTheLoai = TheLoaiController::sharedInstance()->tblMaTheLoai;
            ostringstream oStrStream;
            oStrStream << (*iterator)->maTheLoai;
            TheLoai* theLoai = tblMaTheLoai[oStrStream.str().c_str()];
            TheLoaiController::readATypeFromFile(*theLoai);
            vector<string> cacTruong = theLoai->cacTruongCuaTheLoai;
            for (vector<string>::const_iterator jiterator = cacTruong.begin(); jiterator != cacTruong.end(); jiterator++) {
                outStream << (*((*iterator)->tblFieldDetail))[(*jiterator).c_str()] << endl;
            }
        }
        outStream.close();
        outPos.close();
    }
    return true;
}

bool WordController::newWord() {
    Word *word = new Word();
    if (word->nhapWord() == false) {
        return false;
    }
    if (tblWord[word->tu.c_str()]) {
        cout << "Tu nay da ton tai trong tu dien";
        return false;
    }
    tblWord[word->tu.c_str()] = word;
    wordList.push_back(word);
    databaseModified = true;
    return true;
}

void WordController::searchWord(string word) {
    Word *w = tblWord[word.c_str()];
    if (!w) {
        cout << "Khong co tu nay trong tu dien." << endl;
    } else {
        ostringstream oss;
        oss << w->maTheLoai;
        string s = oss.str();
        TheLoai *theLoai = TheLoaiController::sharedInstance()->tblMaTheLoai[oss.str().c_str()];
        TheLoaiController::readATypeFromFile(*theLoai);
        vector<string> cacTruong = theLoai->cacTruongCuaTheLoai;
        string str = cacTruong[0];
        if ((*(w->tblFieldDetail))[cacTruong[0].c_str()] == "") {
            readAWordFromFile(*w);
        }
        cout << "Tu:\t" << w->tu << endl;
        cout << "Ma The Loai:\t" << w->maTheLoai << endl;
        hashtable<TheLoai *> tblMaTheLoai = TheLoaiController::sharedInstance()->tblMaTheLoai;
        for (vector<string>::const_iterator jiterator = cacTruong.begin(); jiterator != cacTruong.end(); jiterator++) {
            cout << (*jiterator) << ":\t" << (*(w->tblFieldDetail))[(*jiterator).c_str()] << endl;
        }
    }
}

bool WordController::modifyWord(string word, string mode) {
    if (!tblWord[word.c_str()])
        return false;
    cout << "Are you sure(y/n)?\t";
    string temp;
    getline(cin, temp);
    if (temp == "y" || temp == "Y") {
        if (mode == kWordModifyKeyChange) {
            int tempMaTheLoai = tblWord[word.c_str()]->maTheLoai;
            Word *w = new Word();
            if (!w->nhapWord(tempMaTheLoai))
                return false;
            tblWord[w->tu.c_str()] = w;
            wordList.push_back(w);
        }
        wordList.erase(remove(wordList.begin(), wordList.end(), tblWord[word.c_str()]));
        tblWord[word.c_str()] = nullptr;
        databaseModified = true;
    }
    return true;
}

bool WordController::removeWordWithMaTheLoai(int maTheLoai) {
    for (vector<Word*>::const_iterator iterator = wordList.begin(); iterator != wordList.end(); iterator++) {
        if ((*iterator)->maTheLoai == maTheLoai) {
            tblWord[(*iterator)->tu.c_str()] = nullptr;
            wordList.erase(iterator);
            iterator--;
        }
    }
    databaseModified = true;
    return true;
}

void WordController::wordStatistic(string &key) {
    if (TheLoaiController::sharedInstance()->tblTheLoai[key.c_str()]) {
        ostringstream oss;
        oss << TheLoaiController::sharedInstance()->tblTheLoai[key.c_str()]->maTheLoai;
        key = oss.str();
    }
    if (TheLoaiController::sharedInstance()->tblMaTheLoai[key.c_str()]) {
        // Have the type
        int soTu = 0;
        for (vector<Word*>::const_iterator iterator = wordList.begin(); iterator != wordList.end(); iterator++) {
            if ((*iterator)->maTheLoai == atoi(key.c_str())) {
                soTu++;
            }
        }
        cout << "The loai " << TheLoaiController::sharedInstance()->tblMaTheLoai[key.c_str()]->tenTheLoai << " co " << soTu << " tu";
    } else {
        // Null
        cout << "Khong co ma the loai nay";
    }
}