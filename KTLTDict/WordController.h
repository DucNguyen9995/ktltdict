//
//  WordController.h
//  KTLT
//
//  Created by Duc Nguyen on 4/27/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#ifndef WordController_h
#define WordController_h

#include "CommonUtils.h"
#include "Word.h"

#define kWordValueFilename                      "InputWordValueFile.txt"
#define kWordDatabase                           "WordDatabase.txt"
#define kWordPositionFile                       "WordPosition.yolo"

#define kWordModifyKeyChange                    "Change"
#define kWordModifyKeyDelete                    "Delete"

class WordController {
public:
    bool databaseModified;
    static WordController *sharedInstance();
    vector<Word*> wordList;
    hashtable<Word *> tblWord;
    bool newWord();
    bool writeToFile();
    void searchWord(string word);
    bool modifyWord(string word, string mode = kWordModifyKeyChange);
    bool removeWordWithMaTheLoai(int maTheLoai);
    void wordStatistic(string &key);
    bool readAWordFromFile(Word &word);
private:
    bool isFirstTimeRun;
    void readAllWord(ifstream &input);
    WordController();
    static WordController* wordController;
};

#endif
