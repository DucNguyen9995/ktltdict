#include <iostream>
#include "WordController.h"
#include "TheLoaiController.h"

using namespace std;

int main(int argc, const char * argv[]) {
    // insert code here...
    bool isRunning = true;
	while (isRunning) {
		TheLoaiController *theLoaiController = TheLoaiController::sharedInstance();
	    WordController *wordController = WordController::sharedInstance();
	    cout << "Lua Chon:" << endl;
	    cout << "\t1. Them Tu" << endl;
	    cout << "\t2. So Tu tong cong trong database" << endl;
	    cout << "\t3. Thoat" << endl;
	    cout << "Lua chon:\t";
	    string temp;
	    getline(cin, temp);
	    int choice = CommonUtils::getIntFromUser("Hay Nhap So", temp);
	    switch (choice) {
	    	case 1:
	    		while(1) {
	    			cout << "Neu ban muon dung, an -100 khi nhap tu roi Enter" << endl;
	    			if (!wordController->newWord())
	    				break;
	    		}
	    		break;
	    	case 2:
	    		cout << wordController->wordList.size() << endl;
	    		break;
	    	case 3:
	    		wordController->writeToFile();
	    		isRunning = false;
	    		break;

	    	default:
	    		break;
	    }
	}
    return 0;
}
