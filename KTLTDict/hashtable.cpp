#include <cstring>
#include <cassert>
#include <vector>
#include <cstdlib>

using namespace std;

#ifndef nullptr
#define  nullptr NULL
#endif

typedef unsigned int uint;

static const unsigned int offset_basis = 2166136261u;
static const unsigned int FNV_PRIME = 16777619u;
#define kTableSize                          65536
#define kBlockSize                          256

template<class V>
class hashtable {
public:
    struct Bucket
    {
        char *key;
        V value;
        Bucket *next;
        
        Bucket() : key(), value(), next() {}
        void setKey(const char *k) {
			key = (char *) malloc(strlen(k) + 1); strcpy(key, k);
        }
    };
    
    uint m_tableSize;
    Bucket *m_table;
    uint m_blockSize;
    uint m_blockPos;
    uint m_numKeys;
    vector<Bucket *> m_blocks;
    
    hashtable(unsigned int tableSize = kTableSize, unsigned int blockSize = kBlockSize) {
        assert((tableSize & (tableSize - 1)) == 0);
        m_table = new Bucket[tableSize];
        m_tableSize = tableSize;
        m_blockPos = m_blockSize = blockSize;
        m_numKeys = 0;
    }
    
    V &operator[](const char *key) {
        uint hash = fnv1Hash(key) & (m_tableSize - 1);
        Bucket *firstBucket = m_table + hash;
        Bucket *b = firstBucket;
        if (b->key)
        {
            do
            {
                if (strcmp(b->key, key) == 0)
                    return b->value;	// Found existing bucket
                b = b->next;
            }
            while (b);
        }
        // Add it
        m_numKeys++;
        if (!firstBucket->key) {
            firstBucket->setKey(key);	// Use bucket in table
            return firstBucket->value;
        }
        // Allocate a new bucket
        if (m_blockPos >= m_blockSize)
        {
            Bucket *block = new Bucket[m_blockSize];
            m_blocks.push_back(block);
            m_blockPos = 0;
        }
        b = m_blocks.back() + m_blockPos;
        m_blockPos++;
        b->setKey(key);
        b->next = firstBucket->next;
        firstBucket->next = b;
        return b->value;
    }
private:
    static uint fnv1Hash(const char *key) {
        uint hash = offset_basis;
        for (const char *s = key; *s; s++) {
            hash = (FNV_PRIME * hash) ^ (*s);
        }
        return hash;
    }
};
