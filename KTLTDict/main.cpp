//
//  main.cpp
//  KTLT
//
//  Created by Duc Nguyen on 4/22/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#include <iostream>
#include "AppController.h"

int main(int argc, const char * argv[]) {
    // insert code here...
    while (AppController::sharedInstance()->isRunning) {
        AppController::sharedInstance()->run();
    }
    return 0;
}
